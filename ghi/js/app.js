

function createCard(name, description, pictureUrl, location, starts, ends) {
    return `

      <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-body-secondary">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">${starts}-${ends}</div>
      </div>
    `;

  }



window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
          // Figure out what to do when the response is bad
          console.error('Bad response!')
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const detail = await detailResponse.json();
                const name = detail.conference.name;
                const description = detail.conference.description;
                const location = detail.conference.location;
                const pictureUrl = detail.conference.location.picture_url;
                const starts = new Date(detail.conference.starts).toLocaleDateString();
                const ends = new Date(detail.conference.ends).toLocaleDateString();
                const html = createCard(name, description, pictureUrl, location, starts, ends);
                const column = document.querySelector('.row');
                column.innerHTML += html;


              }

          }

        }
      } catch (e) {
        // Figure out what to do if an error is raised
        const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
        const alertTrigger = document.getElementById('liveAlertBtn')

        function alert(message, type) {
        const wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

        alertPlaceholder.append(wrapper)
        }

        if (alertTrigger) {
        alertTrigger.addEventListener('click', function () {
            alert('Nice, you triggered this alert message!', 'success')
        })
        }

      }

});
